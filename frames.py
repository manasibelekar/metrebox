import cv2
import argparse

print(cv2.__version__)

count = 0
success = True

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
  help="path to input video")
args = vars(ap.parse_args())
vidcap = cv2.VideoCapture(args["video"])
success,image = vidcap.read()

while success:
  success,image = vidcap.read()
  print ('Read a new frame: ', success)
  cv2.imwrite("frame%d.jpg" % count, image)     # save frame as JPEG file
  count += 1
