import os
import re
import collections
import numpy as np

directory = "./log"
count  = 0
frame_count = 1
arr = []
my_dict = dict()
for filename in os.listdir(directory):
  frame_num = filename.split(".")[0].split("frame")[1]
  filename = os.path.abspath(os.path.join(directory, filename))
  _rex = re.compile(r"(.*)(bicycle)(.*)")
  with open(filename, "r") as ins:
    intermediate = []
    for line in ins:
      m = _rex.match(line)
      if (m != None):
        words = line.split(": ")
        confidence = words[1].split("%")[0]
        intermediate.append(float(confidence))
        my_dict[int(frame_num)] = intermediate
        arr.append(intermediate)
  frame_count = frame_count + 1
od = collections.OrderedDict(sorted(my_dict.items()))
sorted_arr = []
for k, v in od.items():
  sorted_arr.append(v)
flattened  = [val for sublist in sorted_arr for val in sublist]

for i in range(len(flattened) - 1):
  if flattened[i] > 80 and flattened[i-1] <=70 and flattened[i+1] < flattened[i]:
    count = count + 1
print ("Total number of bicycles: " + str(count))




